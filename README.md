# XML Data Compare REST Java CLI

This sample command-line driver can be used to experiment with XML Data Compare's REST API. Please note that the REST server must be running for this code to operate. This client connects to `http://localhost:8080` by default, but you can use the property "host" to customize the host, e.g. `-Dhost=http://localhost:1234` to point to the REST service running on a different port to the default of 8080. 

This command-line driver was written to demonstrate the use of the XML Data Compare REST API, and is not intended as a primary way of interacting with it. Please customize the source code as needed. Currently, it uses asynchronous calls with multipart/form-data. This sample processes responses as Strings and uses XPath to get information from the responses.

## Start the REST server

Before running the command-line driver, the XML Data Compare REST server must be started. See the web page [REST On-premise Installation and Setup Guide](https://docs.deltaxml.com/xdc/latest/rest-user-guide/rest-on-premise-installation-and-setup-guide) for details on how to run the server.
    
## List the Available Commands

`java -jar deltaxml-data-compare-client-1.0.0.jar`    
    
```text
Usage:
java -jar deltaxml-data-compare-client-x.y.z.jar
java -Dhost=http://localhost:1234 -jar deltaxml-data-compare-client-x.y.z.jar
java -jar deltaxml-data-compare-client-x.y.z.jar compare <inputAFile> <inputBFile> <configFile> <resultFile> <paramName=value>
```   

## Parameters

An optional boolean parameter `diagnosticsMode` can be specified to control information provided about the runtime XPath errors along with a location in the configuration file.    
Set this to false once a comparison is running satisfactorily to speed up the process.    
By default, the `diagnosticsMode` is set to true.   
    
## Run Data Compare Example
    
`java -jar deltaxml-data-compare-client-1.0.0.jar  compare  resources/personA.xml  resources/personB.xml  resources/person-config.xml  resources/result.xml`    
    
Output:    
```text
     
     
XML Data Compare REST command-line driver. (C) 2019 DeltaXML Ltd.  All rights reserved.   
    
Progress:  
       STARTED
       PROCESSING
               Inputs Loading -'input-a'
               Inputs Loading -'input-b'
               Comparison in progress
               Output processing in progress

       SUCCESS

Result written to resources/result.xml
```    
     
## Reference

For more information about how to write the configuration file, visit the product documentation at [User Guide](https://docs.deltaxml.com/xdc/latest/user-guide)


