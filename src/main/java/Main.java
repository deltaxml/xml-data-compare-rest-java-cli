// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.StatusType;
import javax.xml.transform.stream.StreamSource;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XPathSelector;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmNode;

/**
 * An example of a client using the XML Data Compare REST API from commandline. The requests/responses are handled
 * as XML Strings (and processed using XPath queries).
 *
 * The REST server must be running for this code to operate.  It connects to localhost for the REST service.
 * The host can be configured using system property 'host'.
 */
public class Main {

	// commands
	private static final String COMPARE = "compare";
	
	private static WebTarget target;
	private static Processor p;
	
	private static int argsLength=0;
	private static int firstParamIndex;
	private static int paramCount;
	private static int resultFileLocation;
	private static String workingDirectory;

	public static void main(String[] args) throws Exception {

		Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();

		String host = "http://0.0.0.0:8080";
		if (System.getProperty("host") != null) {
			host = System.getProperty("host");
		}

		target = client.target(host + "/api/xml-data-compare/v1");
		p = new Processor(false);
		
        System.out.println("\n XML Data Compare REST command-line driver. (C) 2019 DeltaXML Ltd.  All rights reserved.\n");

		workingDirectory = System.getProperty("user.dir");

		//first argument must be compare
		if (args.length == 0) {
			usage();
		} else {
			String command = args[0].toLowerCase();
			switch (command) {
			case COMPARE:
				runCompare(target, p, args);
				break;
			default:
				System.err.println("\n ERROR: Unknown command '" + command + "'");
				usage();
				System.exit(1);
			}
		}
	}

	/**
	 * Usage to System.out
	 */
	private static void usage() {

		System.out.println();
		System.out.println(" Usage:");
		System.out.println(" java -jar deltaxml-data-compare-client-x.y.z.jar");
		System.out.println(" java -Dhost=http://localhost:1234 -jar deltaxml-data-compare-client-x.y.z.jar");
		System.out.println(" java -jar deltaxml-data-compare-client-x.y.z.jar compare <inputAFile> <inputBFile> <configFile> <resultFile> <paramName=value>");
		System.out.println();
		System.out.println(" An optional boolean parameter 'diagnosticsMode' can be specified to control information provided about the runtime XPath errors along with a location in the configuration file."
							+ "\n Set this to false once a comparison is running satisfactorily to speed up the process." 
							+ "\n By default, the 'diagnosticsMode' is set to true.");
		System.out.println();
		System.out.println(" For more information about how to write configuration file, visit product documentation at https://docs.deltaxml.com/xml-data-compare/latest/user-guide");
		System.out.println();
	}
	
	/**
	 * This function runs the data comparison using POST
	 * 
	 * @param target client info
	 * @param p saxon processor
	 * @param args arguments from the commandline
	 * @throws Exception
	 */
	private static void runCompare(WebTarget target, Processor p, String[] args) throws Exception {
		
		//validate arguments
		checkArguments(args);
		
		//get multipart data for a multiPart form request
		MultiPart multiPart = getMultipartEntity(args);
        
		//submitting comparison reguest
		Response response = target.path("/compare").request().post(Entity.entity(multiPart, multiPart.getMediaType()));
        
        if (response.getStatus() == 202) {
        	//comparison request accepted
        	URI jobURI = response.getLocation();
            String jobSuffix = jobURI.getPath().replace("/api/xml-data-compare/v1/", "");
            boolean finished = false;
            String previousState = "";
            String previousPhaseDescription = "";
            String jobState = null;
            String phaseDescription = null;
            
            System.out.print("\n Progress:  \n");
            
            while (!finished) {
            	//getting corresponding job for the comparison request
                response =getResponse(jobSuffix, target);
                String jobResponse = response.readEntity(String.class);
                XdmNode jobResult = p.newDocumentBuilder().build(new StreamSource(new StringReader(jobResponse)));
                
                //extracting jobStatus and phaseDescription from the job response
                jobState = getStringByXPath(p, jobResult, "/job/jobStatus");
                if (jobResponse.contains("phaseDescription")) {
                	phaseDescription = getStringByXPath(p, jobResult, "/job/phaseDescription");
                }
                finished = "SUCCESS".equals(jobState) || "FAILED".equals(jobState);
                if (!previousState.equals(jobState) && !jobState.equals("FAILED")) {
                	System.out.println("	" + jobState);
                    previousState = jobState;
                }
                if (phaseDescription!=null && !previousPhaseDescription.equals(phaseDescription)) {
                	System.out.println("		" + phaseDescription);
                	previousPhaseDescription = phaseDescription;
                }
            }
            
            System.out.println();
            
            if ("FAILED".equals(jobState)) {
                System.out.println(" COMPARISON FAILED.");
                
                //getting corresponding job for the comparison request
                response =getResponse(jobSuffix, target);
                String xmlResult = response.readEntity(String.class);
                XdmNode resultTree = p.newDocumentBuilder().build(new StreamSource(new StringReader(xmlResult)));
                
                //extracting error message from the job response
                String errorMessage = getStringByXPath(p, resultTree, "/job/error/message");
                //The errorID may be used to match up the error reported here with a server log entry
                String errorId = getStringByXPath(p, resultTree, "/job/error/errorId");
                System.err.printf("\n %-20s\n\n", "ERROR: id="+ errorId+": "+ errorMessage);
                System.exit(1);
            } else {

            	//Get the Job response to get it's JobId, we then use that to download the response
                response =getResponse(jobSuffix, target);
                String xmlResult = response.readEntity(String.class);
                XdmNode resultTree = p.newDocumentBuilder().build(new StreamSource(new StringReader(xmlResult)));
                String jobId = getStringByXPath(p, resultTree, "/job/jobId");
                
                //Now download the result into the specified file
                boolean downloadedOkay = getResult(target, jobId, new File( args[resultFileLocation]));
                
                //Cleanup the Job on the server; if the download failed for a transient reason 
                // you could try downloading again before deleting the job
				deleteJob(target, jobId);
				if(downloadedOkay) {
					System.exit(0);
				}
				else {
					System.exit(1);
				}
            }
            
        } else {
        	//comparison request failed
        	System.err.println(" Comparison failed with error code " + response.getStatus());
        	String xmlResult = response.readEntity(String.class);
        	XdmNode resultTree = p.newDocumentBuilder().build(new StreamSource(new StringReader(xmlResult)));
            String errorMessage = getStringByXPath(p, resultTree, "/*/message");
            System.err.printf("\n %-20s", "Errors Summary: " + errorMessage);
            long errorCount = getLongByXPath(p, resultTree, "count(/*/errors/*)");
            
            for (long l = 1; l <= errorCount; l++) {
                String error = getStringByXPath(p, resultTree, "/*/errors/error[" + l + "]");
                System.err.printf("\n %s. ERROR: %-20s", l, error);	
            }
            System.out.println("\n\n");	
			System.exit(1);
        }
	}

	/**
	 * This functions returns the multipart entity for a comparison request
	 * 
	 * @param args arguments from the commandline
	 * @return the multipart entity
	 */
	private static MultiPart getMultipartEntity(String[] args) throws Exception {
		List<FormDataBodyPart> formDataBodyPartList= new ArrayList<>();
		
		String file1 = args[1];
		String file2 = args[2];
		String config = args[3];
		
		File f1 = new File(file1);
		File f2 = new File(file2);
		File configFile = new File(config);
		
		formDataBodyPartList.add(new FormDataBodyPart("inputA", f1, MediaType.APPLICATION_XML_TYPE));
		formDataBodyPartList.add(new FormDataBodyPart("inputB", f2, MediaType.APPLICATION_XML_TYPE));
		formDataBodyPartList.add(new FormDataBodyPart("configuration", configFile, MediaType.APPLICATION_XML_TYPE));
		
		//adding parameters
		if(paramCount > 0) {
			for (int i=firstParamIndex; i < argsLength; i++) {
				String[] param=args[i].split("=");
				checkParameter(param);
				formDataBodyPartList.add(new FormDataBodyPart(param[0], param[1], MediaType.TEXT_PLAIN_TYPE));
			}
		}
		
		MultiPart multiPart = new MultiPart(MediaType.MULTIPART_FORM_DATA_TYPE);

		for (FormDataBodyPart formDataBodyPart : formDataBodyPartList) {
			multiPart.bodyPart(formDataBodyPart);
		}

		//Check if result file is not an absolute path, and if not then add working directory (otherwise written to working directory of XDC server)
		if(!args[resultFileLocation].contains(workingDirectory) ){
			args[resultFileLocation] = workingDirectory + "/"+ args[resultFileLocation];
		}
		
		//asynchronous comparison request
		multiPart.bodyPart(new FormDataBodyPart("async", "true", MediaType.TEXT_PLAIN_TYPE));
		return multiPart;
	}

	/**
	 * This functions validates the parameters passed from commandline
	 * 
	 * @param param a parameter array of size 2 with its name and value
	 * @throws SaxonApiException Exception thrown by Saxon
	 */
	private static void checkParameter(String[] param) throws SaxonApiException {
		if (param.length == 2) {
			if ("".equals(param[0])) {
				 System.err.println("\n ERROR: Parameter name is not present: '" + param[0] + "'");
				 usage();
				 System.exit(1);
			}
			if ("".equals(param[1])) {
				 System.err.println("\n ERROR: Parameter value is not present: '" + param[1] + "'");
				 usage();
				 System.exit(1);
			}
		} else {
			System.err.println("\n ERROR: Parameter and its value should be in the form param=value");
			usage();
			System.exit(1);
		}
		
		//for now there is just one parameter
		if (!"diagnosticsMode".equals(param[0])) {
			System.err.println("\n ERROR: Invalid parameter name: " + param[0]);
			usage();
			System.exit(1);
		}
		
	}
	
	/**
	 * This function checks the arguments passed from the commandline
	 * 
	 * @param args arguments from the commandline
	 * @throws Exception
	 */
	private static void checkArguments(String[] args) throws Exception {
		argsLength = args.length;

		if (argsLength < 4) {
			System.err.println("\n ERROR: Not enough arguments.");
			usage();
			System.exit(1);
		}

		firstParamIndex = getFirstParamIndex(args);
		paramCount = firstParamIndex < 0 ? 0 : (argsLength - firstParamIndex);
		resultFileLocation = firstParamIndex < 0 ? argsLength - 1 : (argsLength - paramCount) - 1;

		if (resultFileLocation < 4) {
			System.err.println("\n ERROR: Not enough arguments or missing result-file argument");
			usage();
			System.exit(1);
		}
	}

	private static int getFirstParamIndex(String[] args) {
		int result = -1;
		int i = -1;
		for (String s : args) {
			i++;
			if (s.contains("=")) {
				result = i;
				break;
			}
		}
		return result;
	}

	/**
     * Gets a string value from a node tree using an XPath
     *
     * @param p saxon processor
     * @param n node tree
     * @param xpath the xpath query
     * @return a string value corresponding to the XPath
     * @throws SaxonApiException Exception thrown by Saxon
     */
	private static String getStringByXPath(Processor p, XdmNode n, String xpath) throws SaxonApiException {
		XPathSelector xps = p.newXPathCompiler().compile(xpath).load();
		xps.setContextItem(n);
		return xps.evaluateSingle().getStringValue();
	}

	/**
     * Gets a long value from a node tree using an XPath
     *
     * @param p saxon processor
     * @param n node tree
     * @param xpath the xpath query
     * @return a string value corresponding to the XPath
     * @throws SaxonApiException Exception thrown by Saxon
     */
	private static long getLongByXPath(Processor p, XdmNode n, String xpath) throws SaxonApiException {
		XPathSelector xps = p.newXPathCompiler().compile(xpath).load();
		xps.setContextItem(n);
		return ((XdmAtomicValue) xps.evaluateSingle()).getLongValue();
	}
	
	/**
    * return HTTP GET method response for a request path 
    * @param path request path
    * @param target client info
    * @return Response
    */
	private static Response getResponse(String path, WebTarget target) {
		return target.path(path).request().accept(MediaType.APPLICATION_XML).get();
	}
	
	private static boolean getResult(WebTarget target, String jobId, File resultFile) {
		Response response =getResponse("downloads/"+jobId, target);
      
		if(response.getStatusInfo().getFamily()==Status.Family.SUCCESSFUL) {
      
			try (InputStream result = (InputStream) response.getEntity();) {
				java.nio.file.Files.copy(result, resultFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException ioex) {
				System.err.println("Failed writing response to " + resultFile);
				ioex.printStackTrace();
				return false;
			}
			System.out.println(" Result written to " + resultFile.getAbsolutePath());
			return true;
		}
        else {
        	System.err.println("Failed getting Job result from "+target.path("downloads/"+jobId).getUri()+": "+response.getStatusInfo());
        	return false;
        }
	}
	/**
	 * Cleanup any resources on the Server. Once deleted they cannot be downloaded again
	 * 
	 * @param target
	 * @param jobId
	 */
	private static void deleteJob(WebTarget target, String jobId) {
		Response response =target.path("jobs/"+jobId).request(MediaType.APPLICATION_XML).method("DELETE");
        if(response.getStatusInfo().getFamily()==Status.Family.SUCCESSFUL) {
        	//System.out.println("\n Job "+jobId+" deleted");
        }
        else {
        	System.err.println("\n Failed DELETE'ing Job result "+target.path("jobs/"+jobId).getUri()+": "+response.getStatusInfo());
        }
	}
}
